```
import (
  "database/sql"
  "bitbucket.org/wastedcode/sql-object"
  "fmt"
)

type ExampleTable struct {
  ExampleTableId uint
  Name string
  Age uint
}

func (exampletable *ExampleTable) getTableName() string {
  return "example_table_name"
}

func (exampletable *ExampleTable) buildFromRow(row *sql.Rows) error {
  return row.Scan(
    &exampletable.ExampleTableId,
    &exampletable.Name,
    &exampletable.Age,
  )
}

func (exampletable *ExampleTable) getSqlColumns() []SqlColumn {
  var sqlcolumn = make([]SqlColumn, 3)
  sqlcolumn[0] = SqlColumn{"example_table_id", exampletable.ExampleTableId == 0, exampletable.ExampleTableId}
  sqlcolumn[1] = SqlColumn{"name", exampletable.Name == "", exampletable.Name}
  sqlcolumn[2] = SqlColumn{"age", exampletable.Age == 0, exampletable.Age}
  return sqlcolumn
}

func main() {
  var datasource,_ = sqlobject.NewMySqlDataSource("root:password@localhost/test")
  var example = ExampleTable{}
  example.Age = 10
  records, err := datasource.Find(&example)

  if err != nil {
    return
  }

  for _, record := range records {
    fmt.Printf("%-v\n", record)
  }
}
```
