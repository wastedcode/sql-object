package sqlobject

import (
	"bytes"
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
	"reflect"
	"strings"
)

type SqlColumn struct {
	name       string
	is_default bool
	value      interface{}
}

type Record interface {
	getTableName() string
	getSqlColumns() []SqlColumn
	buildFromRow(*sql.Rows) error
}

// This is the data source that can read/write objects
type SqlDataSource struct {
	db *sql.DB
}

// Creates a new data source using the given dsn
func NewMySqlDataSource(dsn string) (SqlDataSource, error) {
	var datasource SqlDataSource
	var err error
	datasource.db, err = sql.Open("mysql", dsn)
	return datasource, err
}

func (datasource *SqlDataSource) Insert(record Record) error {
	return nil
}

func (datsource *SqlDataSource) Update(criteria Record, record Record) error {
	return nil
}

func (datasource *SqlDataSource) Find(record Record) ([]Record, error) {
	var criteria = record.getSqlColumns()
	var query = datasource.getFindQuery(record.getTableName(), criteria)
	var results = []Record{}

	rows, err := datasource.db.Query(query, datasource.getColumnValues(criteria)...)
	if err != nil {
		return results, err
	}

	for rows.Next() {
		clone := datasource.clone(record)
		err := clone.buildFromRow(rows)
		if err != nil {
			return []Record{}, err
		}
		results = append(results, clone)
	}

	return results, err
}

func (datsource *SqlDataSource) clone(record Record) Record {
	val := reflect.ValueOf(record)
	if val.Kind() == reflect.Ptr {
		val = reflect.Indirect(val)
	}
	return reflect.New(val.Type()).Interface().(Record)
}

func (datasource *SqlDataSource) getFindQuery(table_name string, criteria []SqlColumn) string {
	var column_names = datasource.getColumnNames(criteria, "", false)
	var query bytes.Buffer
	query.WriteString("SELECT ")
	query.WriteString(column_names)
	query.WriteString(" FROM ")
	query.WriteString(table_name)
	query.WriteString(" WHERE ")
	query.WriteString(datasource.getColumnNames(criteria, " = ? ", true))
	return query.String()
}

func (datasource *SqlDataSource) getColumnNames(criteria []SqlColumn, suffix string, skip_default bool) string {
	var keys []string
	for _, column := range criteria {
		if skip_default && column.is_default {
			continue
		}
		keys = append(keys, column.name+" "+suffix+" ")
	}

	return strings.Join(keys, ", ")
}

func (datasource *SqlDataSource) getColumnValues(criteria []SqlColumn) []interface{} {
	var values []interface{}
	for _, column := range criteria {
		if !column.is_default {
			values = append(values, column.value)
		}
	}
	return values
}
